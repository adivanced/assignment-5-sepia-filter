#include <stdint.h>

void sepia_filter(void* oimg, void* iimg, uint64_t padbytes, uint32_t height, uint32_t width){
	float tmpR;
	float tmpG;
	float tmpB;

	for(uint64_t j = 0; j < height; j++){
		for(uint64_t i = 0; i < width; i++){
			tmpR = *((uint8_t*)iimg) *0.393f  + *((uint8_t*)iimg+1) *0.769f + *((uint8_t*)iimg+2) *0.189f ;
			tmpG = *((uint8_t*)iimg) *0.349f  + *((uint8_t*)iimg+1) *0.686f + *((uint8_t*)iimg+2) *0.168f ;
			tmpB = *((uint8_t*)iimg) *0.272f  + *((uint8_t*)iimg+1) *0.534f + *((uint8_t*)iimg+2) *0.131f ;

			if(tmpR > 255.0f){
				*((uint8_t*)oimg) = 255;
			}else{
				*((uint8_t*)oimg) = (uint8_t)tmpR;
			}
			if(tmpG > 255.0f){
				*((uint8_t*)oimg+1) = 255;
			}else{
				*((uint8_t*)oimg+1) = (uint8_t)tmpG;
			}
			if(tmpB > 255.0f){
				*((uint8_t*)oimg+2) = 255;
			}else{
				*((uint8_t*)oimg+2) = (uint8_t)tmpB;
			}
			oimg += 3;
			iimg += 3;
		}
		oimg += padbytes;
		iimg += padbytes;
	}
}
