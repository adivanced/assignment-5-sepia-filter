#include "img.h"
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

/* Please note that this is a very basic and ugly example of working with bmp files.
   I just needed it to be more or less identical to the ASM version for better perfomance measurements*/

int main(int argc, char** argv){
	if(argc != 3){
		goto some_err;
	}


	uint32_t fdi;
	uint32_t fdo;

	if((fdi = open(argv[1], 0)) == -1){goto some_err;}

	char rdbuf[6];

	if(read(fdi, rdbuf, 6) != 6){goto some_err;}

	if( *((uint16_t*)rdbuf) != 0x4D42){goto some_err;}

	lseek(fdi, 0, 0);

	void* iimg = mmap(0, *((uint32_t*)(rdbuf+2)), 1, 2, fdi, 0);
	if(iimg == NULL){goto some_err;}

	if((fdo = open(argv[2], 0x0042, 0x1b6)) == -1){goto some_err;}

	if(ftruncate(fdo, *((uint32_t*)(rdbuf+2))) == -1){
		goto some_err;
	}

	void* oimg = mmap(0, *((uint32_t*)(rdbuf+2)), 2, 3, fdo, 0);
	if(oimg == NULL){goto some_err;}

	memcpy(oimg, iimg, *((uint32_t*)(((char*)iimg)+0xa)));

	uint64_t padding = 0;

	if((*((uint32_t*)(((char*)iimg)+0x12))*3)%4){padding = 4 - (*((uint32_t*)(((char*)iimg)+0x12))*3)%4;}	

	sepia_filter(oimg+*((uint32_t*)(((char*)iimg)+0xa)), iimg+*((uint32_t*)(((char*)iimg)+0xa)), padding, *((uint32_t*)(((char*)iimg)+0x12)), *((uint32_t*)(((char*)iimg)+0x16)));
	sepia_filter(oimg+*((uint32_t*)(((char*)iimg)+0xa)), iimg+*((uint32_t*)(((char*)iimg)+0xa)), padding, *((uint32_t*)(((char*)iimg)+0x12)), *((uint32_t*)(((char*)iimg)+0x16)));
	sepia_filter(oimg+*((uint32_t*)(((char*)iimg)+0xa)), iimg+*((uint32_t*)(((char*)iimg)+0xa)), padding, *((uint32_t*)(((char*)iimg)+0x12)), *((uint32_t*)(((char*)iimg)+0x16)));
	sepia_filter(oimg+*((uint32_t*)(((char*)iimg)+0xa)), iimg+*((uint32_t*)(((char*)iimg)+0xa)), padding, *((uint32_t*)(((char*)iimg)+0x12)), *((uint32_t*)(((char*)iimg)+0x16)));
	sepia_filter(oimg+*((uint32_t*)(((char*)iimg)+0xa)), iimg+*((uint32_t*)(((char*)iimg)+0xa)), padding, *((uint32_t*)(((char*)iimg)+0x12)), *((uint32_t*)(((char*)iimg)+0x16)));
	sepia_filter(oimg+*((uint32_t*)(((char*)iimg)+0xa)), iimg+*((uint32_t*)(((char*)iimg)+0xa)), padding, *((uint32_t*)(((char*)iimg)+0x12)), *((uint32_t*)(((char*)iimg)+0x16)));
	sepia_filter(oimg+*((uint32_t*)(((char*)iimg)+0xa)), iimg+*((uint32_t*)(((char*)iimg)+0xa)), padding, *((uint32_t*)(((char*)iimg)+0x12)), *((uint32_t*)(((char*)iimg)+0x16)));
	sepia_filter(oimg+*((uint32_t*)(((char*)iimg)+0xa)), iimg+*((uint32_t*)(((char*)iimg)+0xa)), padding, *((uint32_t*)(((char*)iimg)+0x12)), *((uint32_t*)(((char*)iimg)+0x16)));
	sepia_filter(oimg+*((uint32_t*)(((char*)iimg)+0xa)), iimg+*((uint32_t*)(((char*)iimg)+0xa)), padding, *((uint32_t*)(((char*)iimg)+0x12)), *((uint32_t*)(((char*)iimg)+0x16)));
	sepia_filter(oimg+*((uint32_t*)(((char*)iimg)+0xa)), iimg+*((uint32_t*)(((char*)iimg)+0xa)), padding, *((uint32_t*)(((char*)iimg)+0x12)), *((uint32_t*)(((char*)iimg)+0x16)));

	return 0;


	some_err:
	return 1;


}