[DEFAULT ABS]
[BITS 64]
global sepia_filter

;tr = 0.393R + 0.769G + 0.189B
;tg = 0.349R + 0.686G + 0.168B
;tb = 0.272R + 0.534G + 0.131B

section .data

align 16
red_coefficients dd 0.393, 0.349, 0.272, 0.0   ; will be in xmm8
align 16
grn_coefficients dd 0.769, 0.686, 0.534, 0.0   ; will be in xmm9
align 16
blu_coefficients dd 0.189, 0.168, 0.131, 0.0   ; will be in xmm10
align 16
maxvalues dd 255, 255, 255, 0

; | r | r | r | 0 | - xmm0
; mulps 
; |rk1|rk2|rk3| 0 | - xmm8

; | g | g | g | 0 | - xmm1
; mulps 
; |gk1|gk2|gk3| 0 | - xmm9

; | b | b | b | 0 | - xmm2
; mulps 
; |bk1|bk2|bk3| 0 | - xmm10

; addps xmm0, xmm1
; addps xmm0, xmm2


;xmm0 - as floats |rred|rgreen|rblue|0|
;                   ||     ||    ||
;                  r*rk1 r*rk2 r*rk3
;                    +     +     +
;                  g*gk1 g*gk2 g*gk3
;                    +     +     +
;                  b*bk1 b*bk2 b*bk3
;



; a copy cycle+sepia filter function
; arguments: si - source buffer, di - dest buffer, r15 - padding size, rdx - width, rbx - height
align 8
sepia_filter:
	movaps  xmm8, [red_coefficients]  ; - 9 bytes
	movaps  xmm9, [grn_coefficients]  ; - 9 bytes
	movaps xmm10, [blu_coefficients]  ; - 9 bytes
	movaps xmm11, [maxvalues]         ; - 9 bytes

	mov rbp, rdx ; width counter      ; - 3 bytes, total of 39 bytes. ._loop turns out to be misaligned
	nop          ; a no opetation instruction, totals the size of the before-loop function's section to 40, making the ._loop aligned
	._loop:
		pxor xmm0, xmm0
		pinsrb xmm0, [rsi], 0   ; IDK why, but for some reason, even though every document with instruction latency measurements
		pinsrb xmm0, [rsi], 4   ; tells you that pinsrb r128, r8, imm8 is 4 times faster than pinsrb r128, m8, imm8
		pinsrb xmm0, [rsi], 8   ; But on my i5 10300h(comet lake) it is quite the opposite - ~10 times slower!!!
		cvtdq2ps xmm0, xmm0
		mulps xmm0, xmm8
		pxor xmm1, xmm1
		pinsrb xmm1, [rsi+1], 0
		pinsrb xmm1, [rsi+1], 4
		pinsrb xmm1, [rsi+1], 8		
		cvtdq2ps xmm1, xmm1
		mulps xmm1, xmm9
		pxor xmm2, xmm2
		pinsrb xmm2, [rsi+2], 0
		pinsrb xmm2, [rsi+2], 4
		pinsrb xmm2, [rsi+2], 8
		cvtdq2ps xmm2, xmm2
		mulps xmm2, xmm10

		addps xmm0, xmm1
		addps xmm0, xmm2

		cvtps2dq xmm0, xmm0
		pminud xmm0, xmm11

		pextrb byte [rdi], xmm0, 0
		pextrb byte [rdi+1], xmm0, 4
		pextrb byte [rdi+2], xmm0, 8

		add rsi, 3
		add rdi, 3
		dec rbp
	jnz ._loop
		mov rbp, rdx
		add rsi, r15
		add rdi, r15
		dec rbx
	jnz ._loop
ret

