[DEFAULT ABS]
[BITS 64]
global _start
extern sepia_filter

section .data
	fdi dq 0
	fdo dq 0

	signature dw 0
	size dd 0

	immapptr dq 0


section .text
_start:

	cmp qword [rsp], 3
	jne _err_wrongargnum

	mov rax, 2              			; sys_open
	mov rdi, qword [rsp+16]
	xor rsi, rsi
	syscall
	cmp rax, 0
	jl _openerr_1
	mov qword [fdi], rax

	xor rax, rax 		    			; sys_read
	mov rdi, qword [fdi]
	mov rsi, signature
	mov rdx, 6 
	syscall

	cmp word [signature], 0x4D42 
	jnz _not_bmperr

	mov rax, 8 							; lseek
	mov rdi, qword [fdi] 				
	xor rsi, rsi
	xor rdx, rdx
	syscall
 
	mov rax, 9 							; mmap
	xor rdi, rdi
	movsx rsi, dword [size]
	mov rdx, 1
	mov r10, 2
	mov r8, qword [fdi]
	xor r9, r9
	syscall

	cmp rax, -1
	je _mmaperr_1

	mov qword [immapptr], rax

	mov rax, 2       					; sys_open
	mov rdi, qword [rsp+24]
	mov rsi, 0x0042  		; O_WRONLY + O_CREAT
	mov rdx, 0x1B6
	syscall
	cmp rax, 0
	jl _openerr_2
	mov qword [fdo], rax

	mov rax, 77 						; ftruncate
	mov rdi, qword [fdo]
	movsx rsi, dword [size]
	syscall

	mov rax, 9 							; mmap
	xor rdi, rdi
	movsx rsi, dword [size]
	mov rdx, 2
	mov r10, 3
	mov r8, qword [fdo]
	xor r9, r9
	syscall 
	cmp rax, -1
	je _mmaperr_2

	;could add the 16-bit bmp support but too lazy

	mov rsi, qword [immapptr]
	movsx r15, dword [rsi+0x12]
	lea r15, [r15*2+r15]

	xor r14, r14
	test r15, 0x3
	jz _paddingdone
	mov r14, r15
	or r14, 0x3
	inc r14
	sub r14, r15

	_paddingdone:
	mov r15, r14

	mov ecx, dword [rsi+0xa]
	movsx rdx, dword [rsi+0x12]
	movsx rbx, dword [rsi+0x16]


	mov rdi, rax
	rep movsb   ; now rdi and rsi point to the beginning of the pixeldata section

	push rsi ; I know, that this way of preserving registers between functions is pretty bad and ugly
	push rdi ; But it is the way gcc does it with the -O3, so for better perfomance measurements, I had to copy it.
	push rdx
	push rbx
	push r15
	call sepia_filter
	mov rsi, qword [rsp+32]
	mov rdi, qword [rsp+24]
	mov rdx, qword [rsp+16]
	mov rbx, qword [rsp+8]
	mov r15, qword [rsp]
	call sepia_filter
	mov rsi, qword [rsp+32]
	mov rdi, qword [rsp+24]
	mov rdx, qword [rsp+16]
	mov rbx, qword [rsp+8]
	mov r15, qword [rsp]
	call sepia_filter
	mov rsi, qword [rsp+32]
	mov rdi, qword [rsp+24]
	mov rdx, qword [rsp+16]
	mov rbx, qword [rsp+8]
	mov r15, qword [rsp]
	call sepia_filter
	mov rsi, qword [rsp+32]
	mov rdi, qword [rsp+24]
	mov rdx, qword [rsp+16]
	mov rbx, qword [rsp+8]
	mov r15, qword [rsp]
	call sepia_filter
	mov rsi, qword [rsp+32]
	mov rdi, qword [rsp+24]
	mov rdx, qword [rsp+16]
	mov rbx, qword [rsp+8]
	mov r15, qword [rsp]
	call sepia_filter
	mov rsi, qword [rsp+32]
	mov rdi, qword [rsp+24]
	mov rdx, qword [rsp+16]
	mov rbx, qword [rsp+8]
	mov r15, qword [rsp]
	call sepia_filter
	mov rsi, qword [rsp+32]
	mov rdi, qword [rsp+24]
	mov rdx, qword [rsp+16]
	mov rbx, qword [rsp+8]
	mov r15, qword [rsp]
	call sepia_filter
	mov rsi, qword [rsp+32]
	mov rdi, qword [rsp+24]
	mov rdx, qword [rsp+16]
	mov rbx, qword [rsp+8]
	mov r15, qword [rsp]
	call sepia_filter
	mov rsi, qword [rsp+32]
	mov rdi, qword [rsp+24]
	mov rdx, qword [rsp+16]
	mov rbx, qword [rsp+8]
	mov r15, qword [rsp]
	call sepia_filter

	mov rax, 60
	xor rdi, rdi
	syscall
	
	_err_wrongargnum:
	_mmaperr_2:
	_openerr_2:
	_mmaperr_1:
	_openerr_1:
	_not_bmperr:

	mov rax, 60
	mov rdi, 1
	syscall

